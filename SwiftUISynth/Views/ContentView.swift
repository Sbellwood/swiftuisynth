//
//  ContentView.swift
//  SwiftUISynth
//
//  Created by Skyler Bellwood on 7/1/20.
//  Copyright © 2020 Skyler Bellwood. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @EnvironmentObject var audioKitService: AudioKitService
    @State private var isDemoLoaded = false
    
    var buttons: [RadialButton] {
        [
            RadialButton(
                label: "Sound",
                image: Image(systemName: audioKitService.isSoundEnabled ? "speaker.3.fill" : "speaker.slash.fill"),
                action: audioKitService.toggleSound
            ),
            RadialButton(
                label: "Play",
                image: Image(systemName: audioKitService.areSliderMovementsPlayed ? "square.fill" : "play.fill"),
                action: audioKitService.play
            ),
            RadialButton(
                label: "Record",
                image: Image(systemName: audioKitService.areSliderMovementsRecorded ? "square.fill" : "circle.fill"),
                action: audioKitService.record
            )
        ]
    }
    
    var body: some View {
        
        NavigationView {
            ZStack(alignment: .bottomTrailing) {
                Form {
                    Section(header: Text("Amplitude (0 - 5)").font(.headline)) {
                        VStack {
                            Slider(value: $audioKitService.primaryAmplitude, in: 0.0...5.0)
                            Text("\(audioKitService.primaryAmplitude, specifier: "%.2f")")
                        }
                    }
                    
                    Section(header: Text("Frequency (0 - 1500Hz)").font(.headline)) {
                        VStack {
                            Slider(value: $audioKitService.primaryFrequency, in: 0.0...1500.0)
                            Text("\(audioKitService.primaryFrequency, specifier: "%.2f")")
                        }
                    }
                    
                    
                    Section(header: Text("Recordings").font(.headline)) {
                        Button(isDemoLoaded ? "Demo Loaded" : "Load demo") {
                            self.isDemoLoaded.toggle()
                            self.audioKitService.loadDemo()
                        }
                        Button("Delete Recording") {
                            self.isDemoLoaded = false
                            self.audioKitService.delete()
                        }
                    }
                }
                
                RadialMenu(title: "Actions...", closedMenuImage: Image(systemName: "ellipsis.circle"), expandedMenuImage: Image(systemName: "multiply.circle.fill"), buttons: buttons)
                    .offset(x: -20, y: -20)
                    .buttonStyle(CustomButtonStyle())
            }
                
            .navigationBarTitle("Swifty Synth")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
