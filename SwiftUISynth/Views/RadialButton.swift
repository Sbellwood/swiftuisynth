//
//  RadialButton.swift
//  SwiftUISynth
//
//  Created by Skyler Bellwood on 7/8/20.
//  Copyright © 2020 Skyler Bellwood. All rights reserved.
//

import Foundation
import SwiftUI

struct RadialButton {
    let label: String
    let image: Image
    let action: () -> Void
}
