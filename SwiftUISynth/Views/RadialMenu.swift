//
//  RadialMenu.swift
//  SwiftUISynth
//
//  Created by Skyler Bellwood on 7/8/20.
//  Copyright © 2020 Skyler Bellwood. All rights reserved.
//

import Foundation
import SwiftUI

struct RadialMenu: View {
    @State private var isMenuExpanded = false
    @State private var isShowingSheet = false
    
    let title: String
    let closedMenuImage: Image
    let expandedMenuImage: Image
    let buttons: [RadialButton]
    var direction = Angle(degrees: 315)
    var range = Angle(degrees: 90)
    var distance = 100.0
    var animation = Animation.interactiveSpring(response: 0.4, dampingFraction: 0.6, blendDuration: 0.0)
    
    var body: some View {
        ZStack {
            Button(action: {
                if UIAccessibility.isVoiceOverRunning {
                    self.isShowingSheet.toggle()
                } else {
                    self.isMenuExpanded.toggle()
                }
            }) {
                isMenuExpanded ? expandedMenuImage : closedMenuImage
            }
            .accessibility(label: Text(title))
            
            ForEach(0..<buttons.count, id: \.self) { index in
                Button(action: {
                    self.buttons[index].action()
                    self.isMenuExpanded.toggle()
                }) {
                    self.buttons[index].image
                }
                .accessibility(hidden: self.isMenuExpanded == false)
                .accessibility(label: Text(self.buttons[index].label))
                .offset(self.offset(for: index))
            }
            .opacity(self.isMenuExpanded ? 1 : 0)
            .animation(self.animation)
        }
        .actionSheet(isPresented: $isShowingSheet) {
            ActionSheet(title: Text(title), message: nil, buttons:
                buttons.map { button in
                    ActionSheet.Button.default(Text(button.label), action: button.action)
                } + [.cancel()]
            )
        }
    }
    
    func offset(for index: Int) -> CGSize {
        guard isMenuExpanded else { return .zero }
        
        let alignmentAngle = range.radians / Double(buttons.count - 1)
        let buttonAngle = alignmentAngle * Double(index)
        let finalAngle = direction - (range / 2) + Angle(radians: buttonAngle)
        
        let finalX = cos(finalAngle.radians - .pi / 2) * distance
        let finalY = sin(finalAngle.radians - .pi / 2) * distance
        return CGSize(width: finalX, height: finalY)
    }
}
