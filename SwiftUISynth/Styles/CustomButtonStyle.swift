//
//  CustomButtonStyle.swift
//  SwiftUISynth
//
//  Created by Skyler Bellwood on 7/8/20.
//  Copyright © 2020 Skyler Bellwood. All rights reserved.
//

import Foundation
import SwiftUI

struct CustomButtonStyle: ButtonStyle {
    func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .padding()
            .font(.title)
            .background(Color.blue.opacity(configuration.isPressed ? 0.5 : 1))
            .clipShape(Circle())
            .foregroundColor(.white)
    }
}
