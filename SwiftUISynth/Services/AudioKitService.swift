//
//  AudioKitService.swift
//  SwiftUISynth
//
//  Created by Skyler Bellwood on 7/1/20.
//  Copyright © 2020 Skyler Bellwood. All rights reserved.
//

import Foundation
import AudioKit

final class AudioKitService: ObservableObject {
    
    // Singleton
    static let shared = AudioKitService()
    
    // Sets up the AudioKit waveforms
    private let square = AKTable(.square, count: 256)
    private let triangle = AKTable(.triangle, count: 256)
    private let sine = AKTable(.sine, count: 256)
    private let sawtooth = AKTable(.sawtooth, count: 256)

    private var primaryOscillator: AKOscillator
    
    // SwiftUI visible properties
    @Published var isSoundEnabled = false
    @Published var areSliderMovementsRecorded = false
    @Published var areSliderMovementsPlayed = false
    @Published var primaryAmplitude: Double = 0.5 {
        didSet {
            setAmplitudeAndFrequency()
        }
    }
    @Published var primaryFrequency: Double = 220 {
        didSet {
            setAmplitudeAndFrequency()
        }
    }
    
    private var timer = Timer()
    private var recordedAmplitudes = [Double]()
    private var recordedFrequencies = [Double]()
    
    private let demoRecordedFrequencies: [Double] = [
        // 1
        392, 392, 392, 392,
        392, 392, 392, 392,
        
        // 2
        392, 392, 392, 392,
        392, 392, 392, 392,
        
        // 3
        392, 392, 392, 392,
        392, 392, 392, 392,
        
        // 4
        392, 392, 554, 554,
        554, 554, 554, 554,
        
        // 5
        554, 554, 554, 554,
        554, 554, 554, 554,
        
        // 6
        554, 554, 492, 492,
        492, 492, 492, 492,
        
        // 7
        492, 492, 492, 492,
        492, 492, 492, 492,
        
        // 8
        492, 492, 349, 349,
        349, 349, 349, 349
    ]
    private let demoRecordedAmplitudes: [Double] = [
        // 1
        1, 0, 1, 0,
        1, 0, 1, 0,
        
        // 2
        1, 1, 0, 1,
        1, 0, 1, 0,
        
        // 3
        1, 0, 1, 0,
        1, 0, 1, 0,
        
        // 4
        1, 1, 0, 1,
        1, 0, 1, 0,
        
        // 5
        1, 0, 1, 0,
        1, 0, 1, 0,
        
        // 6
        1, 1, 0, 1,
        1, 0, 1, 0,
        
        // 7
        1, 0, 1, 0,
        1, 0, 1, 0,
        
        // 8
        1, 1, 0, 1,
        1, 0, 1, 0
    ]
    
    private var ampIndex = 0
    private var freqIndex = 0
    
    init() {
        
        primaryOscillator = AKOscillator(waveform: sine)
        primaryOscillator.rampDuration = 0
        AudioKit.output = primaryOscillator
        
        do {
            try AudioKit.start()
        } catch {
            assert(false, "Failed to start AudioKit")
        }
    }
    
    func setAmplitudeAndFrequency() {
        primaryOscillator.amplitude = primaryAmplitude
        primaryOscillator.frequency = primaryFrequency
    }
    
    func record() {
        areSliderMovementsPlayed = false
        areSliderMovementsRecorded = !areSliderMovementsRecorded
        timer.invalidate()
        if areSliderMovementsRecorded {
            timer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true) { _ in
                self.recordSliders()
            }
        }
    }
    
    func play() {
        areSliderMovementsRecorded = false
        areSliderMovementsPlayed = !areSliderMovementsPlayed
        timer.invalidate()
        if areSliderMovementsPlayed {
            timer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true) { _ in
                self.moveSliders()
            }
        }
    }
    
    func recordSliders() {
        recordedAmplitudes.append(primaryAmplitude)
        recordedFrequencies.append(primaryFrequency)
    }
    
    func moveSliders() {
        if recordedAmplitudes.count == 0 && recordedFrequencies.count == 0 {
            areSliderMovementsPlayed = false
        }
        
        if areSliderMovementsPlayed {
            if recordedAmplitudes.count != 0 {
                primaryAmplitude = recordedAmplitudes[ampIndex]
                ampIndex += 1
                if ampIndex == recordedAmplitudes.count {
                    ampIndex = 0
                }
            }
            if recordedFrequencies.count != 0 {
                primaryFrequency = recordedFrequencies[freqIndex]
                freqIndex += 1
                if freqIndex == recordedFrequencies.count {
                    freqIndex = 0
                }
            }
            setAmplitudeAndFrequency()
        }
    }
    
    func delete() {
        recordedFrequencies.removeAll()
        recordedAmplitudes.removeAll()
    }
    
    func loadDemo() {
        recordedAmplitudes = demoRecordedAmplitudes
        recordedFrequencies = demoRecordedFrequencies
    }
    
    func toggleSound() {
        if primaryOscillator.isPlaying {
            primaryOscillator.stop()
        } else {
            primaryOscillator.amplitude = primaryAmplitude
            primaryOscillator.frequency = primaryFrequency
            primaryOscillator.start()
        }
        isSoundEnabled = primaryOscillator.isPlaying
    }
}
